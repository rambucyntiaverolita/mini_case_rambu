describe('Create a New User', () => 
  {
    it('Failed Created New User', () => 
    {
      cy.request({method: 'POST', url: 'https://gorest.co.in/public/v2/users', failOnStatusCode: false,
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        } , 
        body: 
        {
        "name": "Brian",
        "email": "Brian1",
        "gender": "male",
        "status": "active"
        }
      }).then((response) => 
      {
        //Asserting the status code to be 422 for Unprocessable Entity
        expect(response.status).to.equal(422)
        //Asserting the body respon is email is invalid
        expect(response.body[0].field).to.equal('email')
        expect(response.body[0].message).to.equal('is invalid')
        
      })
   })
  })