describe('GET User From gorest', () => 
  {
    it('Sucessfully Get User', () => 
    {
      cy.request({method: 'GET', url: 'https://gorest.co.in/public/v2/users', 
      //Authorization Token
      'auth': 
      {
        'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
      }}).then((response) => 
      {
        //Asserting the status code to be 200 for successful response
        expect(response.status).to.eq(200)        
      })
   })
  })