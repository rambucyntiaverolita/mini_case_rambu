describe('Update User', () => 
  {
    it('Sucessfully Update User', () => 
    {
      cy.request({method: 'PUT', url: 'https://gorest.co.in/public/v2/users/6871004', 
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        } , body:{
        "name": "Rachel Hawl",
        "email": "RachelHawl@mail.test",
        "gender": "female",
        "status": "inactive"
      }}).then((response) => 
      {
        //Asserting the status code to be 200 for successful response
        expect(response.status).to.eq(200)
        //Asserting the respon body
        expect(response.body.email).to.eq('RachelHawl@mail.test')
        expect(response.body.name).to.eq('Rachel Hawl')
        expect(response.body.gender).to.eq('female')
        expect(response.body.status).to.eq('inactive')
        expect(response.body.id).to.eq(6871004)
      })
   })
  })