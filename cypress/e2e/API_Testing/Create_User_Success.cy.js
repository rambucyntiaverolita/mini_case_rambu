describe('Create a New User', () => 
  {
    it('Sucessfully Created New User', () => 
    {
      cy.request({method: 'POST', url: 'https://gorest.co.in/public/v2/users', 
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        } , body:{
        "name": "Manda",
        "email": "MandaF@mail.test",
        "gender": "female",
        "status": "active"
      }}).then((response) => 
      {
        //Asserting the status code to be 201 for successful response
        expect(response.status).to.eq(201)
        //Asserting the body respon 
        expect(response.body.name).to.equal('Manda')
        expect(response.body.email).to.equal('MandaF@mail.test')
        expect(response.body.gender).to.equal('female')
        expect(response.body.status).to.equal('active')
      })
   })
  })