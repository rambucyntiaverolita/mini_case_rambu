describe('GET User From gorest', () => 
  {
    it('Failed Get User', () => 
    {
      cy.request({method: 'GET', url: 'https://gorest.co.in/public/v2/users/6870698',failOnStatusCode: false,})
      .then((response) => 
      {
        //Asserting the status code to be 404
        expect(response.status).to.eq(404)
        //Asserting the response body is resource not found 
        expect(response.body.message).to.equal('Resource not found')
      })
   })
  })