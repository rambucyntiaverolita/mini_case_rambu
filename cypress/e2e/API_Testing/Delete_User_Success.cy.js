describe('Delete User', () => 
  {
    it('Successfully Delete User', () => 
    {
      cy.request({method: 'DELETE', url: 'https://gorest.co.in/public/v2/users/6870985',
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        }}).then((response) => 
      {
        //Asserting the status code to be 204 for delete user
        expect(response.status).to.equal(204)              
      })
   })
  })