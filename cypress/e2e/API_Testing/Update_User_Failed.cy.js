describe('Update User', () => 
  {
    it('Sucessfully Update User', () => 
    {
      cy.request({method: 'PUT', url: 'https://gorest.co.in/public/v2/users/6871004', failOnStatusCode: false,
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        } , body:{
        "name": "Rachel Hawl",
        "email": "RachelHawl@mail.test",
        "gender": "other",
        "status": "inactive"
      }}).then((response) => 
      {
        //Asserting the status code to be 200 for successful response
        expect(response.status).to.eq(422)
        //Asserting the respon body
        expect(response.body[0].field).to.eq('gender')
        expect(response.body[0].message).to.eq("can't be blank, can be male of female")
      })
   })
  })