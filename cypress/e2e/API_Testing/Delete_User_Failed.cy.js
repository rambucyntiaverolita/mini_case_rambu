describe('Delete User', () => 
  {
    it('Failed Delete User', () => 
    {
      cy.request({method: 'DELETE', url: 'https://gorest.co.in/public/v2/users/6870698',failOnStatusCode: false,
        //Authorization Token
        'auth': 
        {
          'bearer' : '9cfc1fd2bf2c9e01aa709b6aef6d670e626ed8443905ec5fe3bdf66763ce1f5d'
        }}).then((response) => 
      {
        //Asserting the status code to be 404 if no resource
        expect(response.status).to.equal(404)              
        expect(response.body.message).to.equal('Resource not found')
      })
   })
  })