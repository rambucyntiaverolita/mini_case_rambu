from appium import webdriver
from appium.options.common.base import AppiumOptions
from appium.webdriver.common.appiumby import AppiumBy
import time

# For W3C actions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.actions import interaction
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.pointer_input import PointerInput

options = AppiumOptions()
options.load_capabilities({
	"platformName": "android",
	"appium:platformVersion": "10",
	"appium:deviceName": "Pixel 3 API 29",
	"appium:app": "D:\\Mini Case BNI\\Mobile_Testing\\app\\com.blodhgard.easybudget_21469_apps.evozi.com.apk",
	"appium:automationName": "UiAutomator2",
	"appium:ensureWebviewsHavePages": True,
	"appium:nativeWebScreenshot": True,
	"appium:newCommandTimeout": 3600,
	"appium:connectHardwareKeyboard": True
})

driver = webdriver.Remote("http://127.0.0.1:4724", options=options)

# button ok 
button_ok = driver.find_element(by=AppiumBy.ID, value="com.blodhgard.easybudget:id/button_initial_settings_privacy_save")
button_ok.click()

# verify element collect data is present after click ok
verify_element = driver.find_element(by=AppiumBy.ID, value="com.blodhgard.easybudget:id/radiobutton_app_usage_data_collection_yes")
verify_element.is_displayed()

time.sleep(2)

driver.quit()