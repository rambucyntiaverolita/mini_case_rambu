# Mini_Case_Rambu
Name : Rambu Cyntia Verolita
API Testing GOREST and Mobile Testing Application Easybudget

# Tools

- [ ] Visual Studio Code
- [ ] Appium Inspector
- [ ] Android Studio
- [ ] Cypress

# API_Testing with Cypress
 1. Install cypress on your computer 
 ``` npm install cypress --save-dev ```
 2. Open the project folder with vscode and create the script with extension .cy.js
 3. To run the script, open cmd and go to the path project. open the cypress with code ``` npx cypress open ```
 4. when cypess open, click the E2E testing and choose edge or firefox browser to start E2E testing
 5. After the browser is open, you can select the script that you want to run

# Mobile_Testing with Appium
1. Install appium on your computer 
``` npm i -g appium ```
2. Open the project folder with vscode and create the script with extension.py
3. Before run the script, please start the appium and open the android studio